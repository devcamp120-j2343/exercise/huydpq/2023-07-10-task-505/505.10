// import thư viện ẽxpress

const express = require("express");
// khỏi tạo 1 app express

const app = express();

// khai bao cổng 
const port = 8000;

app.get("/",(req, res) => {
    let today = new Date();

    res.json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()+1} năm ${today.getFullYear()}`
    })
})

app.listen(port, () => {
    console.log("App listening on port: " , port )
})